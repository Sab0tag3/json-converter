package com.rosneft.converter.Model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "document")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(mappedBy = "document")
    private User user;

    private String type;

    @Column(unique = true)
    private int number;
}
