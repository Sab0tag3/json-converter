package com.rosneft.converter.Service;

import com.google.gson.Gson;
import com.rosneft.converter.Model.User;
import com.rosneft.converter.Repository.UserRepository;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;

@Service
public class Converter {

    private final UserRepository repository;

    public Converter(UserRepository repository) {
        this.repository = repository;
        convert();
    }

    public void convert() {
    try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/test.json"))) {
            Gson gson = new Gson();
            User[] users = gson.fromJson(br, User[].class);
            for (User user : users
            ) {
                repository.save(user);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
